import React from "react";
import { NavLink } from "react-router-dom";
type Props = {};

export default function Header(props: Props) {
  return (
    <div className="header">
      <section className="header-logo">
        <div className="logo">
          <NavLink className="logo-link" to="/">
            <img src="./img/logo.png" alt="logo" />
          </NavLink>
        </div>
        <div className="navbar-search ">
          <div className="search flex-item">
            <NavLink to="/search" className="search-link">
              <i className="fa fa-search"></i>
            </NavLink>
          </div>
          <div className="carts flex-item">
            <NavLink to="/carts" className="carts-link">
              <i className="fa fa-cart-plus"></i>
            </NavLink>
          </div>
          <div className="login flex-item">
            <NavLink to="/login" className="login-link">
              Login
            </NavLink>
          </div>
          <div className="register flex-item">
            <NavLink to="/register" className="register-link">
              Register
            </NavLink>
          </div>
        </div>
      </section>
      <section className="menu">
        <nav className="nav-menu">
          <NavLink className="mx-2 nav-item" to="">
            Home
          </NavLink>
          <NavLink className="mx-2  nav-item" to="">
            Men
          </NavLink>
          <NavLink className="mx-2  nav-item" to="">
            Woman
          </NavLink>
          <NavLink className="mx-2  nav-item" to="">
            Kid
          </NavLink>
          <NavLink className="mx-2  nav-item" to="">
            Sport
          </NavLink>
        </nav>
      </section>
    </div>
  );
}

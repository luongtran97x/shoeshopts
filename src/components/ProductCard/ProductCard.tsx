import React from "react";
import { NavLink } from "react-router-dom";
import { ProductModel, RelatedProduct } from "../../redux/ProductSlice";

type Props = {
  prod: ProductModel | RelatedProduct;
};

export default function ProductCard({ prod }: Props) {
  return (
    <div className="card">
      <div className="icon">
        <i className="fa fa-heart"></i>
      </div>
      <img src={prod.image} alt={prod.alias} />
      <div className="card-body">
        <p className="card-title">{prod.name}</p>
        <p>{prod.shortDescription}</p>
      </div>
      <div className="card-footer d-flex">
        <NavLink
          className="btn btn-success w-50 border-none"
          to={`/detail/${prod.id}`}
        >
          BuyNow
        </NavLink>
        <div className="product-price text-center w-50 bg-secondary">
          {prod.price}
        </div>
      </div>
    </div>
  );
}

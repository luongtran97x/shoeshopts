import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DispatchType, RootState } from "../../redux/configStore";
import ProductCard from "../../components/ProductCard/ProductCard";
import {
  DetailItemModel,
  ProductModel,
  getProductApi,
} from "../../redux/ProductSlice";
import { NavLink } from "react-router-dom";

type Props = {};

const Home: React.FC = ({}: Props) => {
  const { arrProduct } = useSelector((state: RootState) => state.ProductSlice);
  console.log("🚀 ~ arrProduct:", arrProduct);
  const dispatch: DispatchType = useDispatch();

  const getAllProductsApi = () => {
    const actionAsync = getProductApi();
    dispatch(actionAsync);
  };

  useEffect(() => {
    getAllProductsApi();
  }, []);

  return (
    <>
      <div
        id="carouselExampleControls"
        className="carousel  slide bg-success"
        data-ride="carousel"
      >
        <div className="carousel-inner ">
          {arrProduct.map((item: DetailItemModel) => {
            return item.id === 1 ? (
              <div key={item?.id} className="carousel-item active">
                <img
                  className="d-block w-100"
                  src={item?.image}
                  alt={item?.alias}
                />
                {/* <NavLink className="" to={`/detail/${item.id}`}>
                  Xem chi tiết
                </NavLink> */}
              </div>
            ): <div key={item?.id} className="carousel-item ">
            <img
              className="d-block w-100"
              src={item?.image}
              alt={item?.alias}
            />
            {/* <NavLink className="col-3" to={`/detail/${item.id}`}>
              Xem chi tiết
            </NavLink> */}
          </div>

          })}
          {/* <div className="carousel-item active">
            <img className="d-block w-100" src="..." alt="First slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="..." alt="Second slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="..." alt="Third slide" />
          </div> */}
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleControls"
          role="button"
          data-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleControls"
          role="button"
          data-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>

      {/* Product Feature */}
      <div className="container">
        <h3>Product Feature</h3>
        <div className="row">
          {arrProduct.map((item: ProductModel, index: number) => {
            return (
              <div className="col-4" key={index}>
                <ProductCard prod={item} />
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};
export default Home;

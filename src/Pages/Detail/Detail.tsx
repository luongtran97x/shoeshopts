import React, { useEffect } from "react";
import ProductCard from "../../components/ProductCard/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import {
  ProductItem,
  RelatedProduct,
  setDetailProduct,
} from "../../redux/ProductSlice";
import { DispatchType, RootState } from "../../redux/configStore";
import { useParams } from "react-router-dom";
import { https } from "../../services/config";

type Props = {};

export default function Detail({}: Props) {
  const dispatch: DispatchType = useDispatch();
  const { id } = useParams();
  const { productDetail } = useSelector(
    (state: RootState) => state.ProductSlice
  );

  useEffect(() => {
    https
      .get(`api/Product/getbyid?id=${id}`)
      .then((res) => {
        const content: ProductItem = res.data.content;
        dispatch(setDetailProduct(content));
      })
      .catch((err) => {});
  }, [id]);

  return (
    <div className="container">
      <div className="row mt-2">
        <div className="col-4">
          <img
            src={productDetail?.image}
            alt={productDetail?.alias}
            width={350}
          />
        </div>
        <div className="col-8">
          <h3>{productDetail?.name}</h3>
          <p>{productDetail?.shortDescription}</p>
        </div>
        <h3>Realte Product</h3>
        <div className="row">
          {productDetail?.relatedProducts.map(
            (item: RelatedProduct, index: number) => {
              return (
                <div key={index}>
                  <ProductCard prod={item} />
                </div>
              );
            }
          )}
        </div>
      </div>
    </div>
  );
}


import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { DispatchType } from "./configStore";
import { https } from "../services/config";


export interface ProductModel {
  id: number;
  name: string;
  alias: string;
  price: number;
  description: string;
  size: string;
  shortDescription: string;
  quantity: number;
  deleted: boolean;
  categories: string;
  relatedProducts: string;
  feature: boolean;
  image: string;
}
export interface ProductItem {
  id: number;
  name: string;
  alias: string;
  price: number;
  feature: boolean;
  description: string;
  size: string[];
  shortDescription: string;
  quantity: number;
  image: string;
  categories: Category[];
  relatedProducts: RelatedProduct[];
}
export interface DetailItemModel{
  id:               number;
  name:             string;
  alias:            string;
  price:            number;
  description:      string;
  size:             string;
  shortDescription: string;
  quantity:         number;
  deleted:          boolean;
  categories:       string;
  relatedProducts:  string;
  feature:          boolean;
  image:            string;
}
export interface Category {
  id:       string;
  category: string;
}

export interface RelatedProduct {
  id:               number;
  name:             string;
  alias:            string;
  feature:          boolean;
  price:            number;
  description:      string;
  shortDescription: string;
  image:            string;
}
export type ProductState = {
  arrProduct: ProductModel[];
  productDetail:ProductItem | null
};

const initialState: ProductState = {
  arrProduct: [],
  productDetail:null
};

const ProductSlice = createSlice({
  name: "ProductSlice",
  initialState,
  reducers: {
    setArrProductApi: (
      state: ProductState,
      action: PayloadAction<ProductModel[]>
    ) => {
      state.arrProduct = action.payload;
    },
    setDetailProduct : (state:ProductState,action:PayloadAction<ProductItem>) => { 
      state.productDetail = action.payload
     }
  },
});

export const { setArrProductApi,setDetailProduct } = ProductSlice.actions;

export default ProductSlice.reducer;

// call api trong slice

export const getProductApi = () => {
  return async (dispatch: DispatchType) => {
    try {
      const result = await https.get("api/Product");
      const content: ProductModel[] = result.data.content;

      // action creator
      const action: PayloadAction<ProductModel[]> = setArrProductApi(content);
      dispatch(action);
    } catch (err) {}
  };
};

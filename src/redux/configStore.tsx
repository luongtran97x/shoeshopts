import {configureStore} from "@reduxjs/toolkit";
import { type } from "@testing-library/user-event/dist/type";
import ProductSlice from "./ProductSlice";



export const store = configureStore({
    reducer:{
        ProductSlice:ProductSlice
    }
})

// sử dụng ultility

export type RootState  = ReturnType<typeof store.getState>


export type DispatchType = typeof store.dispatch
import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes , Route , Navigate} from "react-router-dom";
import HomeTemplate from "./template/HomeTemplate";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";
import Register from "./Pages/Register/Register";
import Detail from "./Pages/Detail/Detail";
import Search from "./Pages/Search/Search";
import Profile from "./Pages/Profile/Profile";
import Carts from "./Pages/Carts/Carts";
// style
import "./assets/scss/style.scss";
// set up redux
import {Provider} from "react-redux"
import { store } from "./redux/configStore";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
  <BrowserRouter>
   <Routes>
      <Route path="" element={<HomeTemplate/>}>
        <Route index element={<Home/>}></Route>
        <Route path="/login" element={<Login/>}></Route>
        <Route path="/register" element={<Register/>}></Route>
        <Route path="/detail/:id" element={<Detail/>}>
        </Route>
        <Route path="/search"  element={<Search/>}></Route>
        <Route path="/profile"  element={<Profile/>}></Route>
        <Route path="/carts"  element={<Carts/>}></Route>
        <Route path="*"  element={<Navigate to="" />}></Route>
      </Route>
   </Routes>
  </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
